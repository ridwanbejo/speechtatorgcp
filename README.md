#Speech Spectator on GCP

Basic Feature:

- Send speech as a text to Cloud Function
- Analyze the sentiment and classification of the text
- Store the result to cloud datastore

Current Schema:

```
cloud function as producer -> cloud pubsub -> natural language service on cloud function -> cloud pubsub -> cloud function as consumer -> cloud data store
```

Sample Payload for the Producer:

```
{
    "userId": "12347",
    "message": "There is still a large gap between the pledges by governments to cut greenhouse gas emissions and the reductions scientists say are needed to avoid dangerous levels of climate change, the UN has said. Current plans from national governments, and pledges made by private sector companies and local authorities across the world, would lead to temperature rises of as much as 3C or more by the end of this century, far outstripping the goal set under the 2015 Paris agreement to hold warming to 2C or less, which scientists say is the limit of safety.",
    "sourceUrl": "http://www.bukan-berita-biasa.com",
    "createdAt": "2018-03-13 17:00:00"
}
```
