// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const Datastore = require('@google-cloud/datastore');

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

// Creates a client
const datastore = new Datastore({
  projectId: projectId,
});

const subscriptionName = 'speechTatorDataSubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);
const uuidv4 = require('uuid/v4');

exports.speechTatorDataConsumer = (event, callback) => {
    // The Cloud Pub/Sub Message object.
    const pubsubMessage = event.data;

    // The kind for the new entity
    const kind = 'speechTatorResults';

    // The name/ID for the new entity
    const name = uuidv4();
    // The Cloud Datastore key for the new entity
    const speechTatorDataKey = datastore.key([kind, name]);

    // Prepares the new entity
    const speechTatorData = {
      key: speechTatorDataKey,
      data: JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString()),
    };

    console.log(speechTatorData);

    // Saves the entity
    datastore
      .save(speechTatorData)
      .then(() => {
        console.log(`Saved ${speechTatorData.key.name}`);
      })
      .catch(err => {
        console.error('ERROR:', err);
      });
  
    // Don't forget to call the callback.
    callback();
};
