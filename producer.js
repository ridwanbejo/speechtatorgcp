// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

exports.speechTatorProducer = (req, res) => {
  if (req.body.message === undefined && req.body.sourceUrl === undefined && req.body.userId === undefined) {
    
    res.status(400).send('Bad message!');
  
  } else {
  
  const dataBuffer = Buffer.from(JSON.stringify(req.body));
  const topicName = "speechTatorAnalyzerPubsub";

  pubsubClient
    .topic(topicName)
    .publisher()
    .publish(dataBuffer)
    .then(results => {
      const messageId = results[0];
      console.log(`Message ${messageId} published.`);
    })
    .catch(err => {
      console.error('ERROR:', err);
    });

    res.status(200).send(req.body);
    
  }
};

