// Imports the Google Cloud client library
const PubSub = require(`@google-cloud/pubsub`);
const language = require('@google-cloud/language');

// Your Google Cloud Platform project ID
const projectId = 'serverlessid';

// Creates a client
const client = new language.LanguageServiceClient({
  projectId: projectId,
});

// Instantiates a client
const pubsubClient = new PubSub({
  projectId: projectId,
});

const subscriptionName = 'speechTatorAnalyzerSubscription';
const timeout = 60;

// References an existing subscription
const subscription = pubsubClient.subscription(subscriptionName);

exports.speechTatorAnalyzer = (event, callback) => {
    // The Cloud Pub/Sub Message object.
    const pubsubMessage = event.data;
    const payload = JSON.parse(Buffer.from(pubsubMessage.data, 'base64').toString());
  console.log (payload);
  
    // Prepares a document, representing the provided text
    const document = {
      content: payload.message,
      type: 'PLAIN_TEXT',
    };

    // Detects the sentiment of the document
    client
      .analyzeSentiment({document: document})
      .then(sentimentResults => {

        /*
        const sentiment = results[0].documentSentiment;
        console.log(`Document sentiment:`);
        console.log(`  Score: ${sentiment.score}`);
        console.log(`  Magnitude: ${sentiment.magnitude}`);

        const sentences = results[0].sentences;
        sentences.forEach(sentence => {
          console.log(`Sentence: ${sentence.text.content}`);
          console.log(`  Score: ${sentence.sentiment.score}`);
          console.log(`  Magnitude: ${sentence.sentiment.magnitude}`);
        });
        */

        client
          .classifyText({document: document})
          .then(results => {
            const classificationResult = results[0];

            /*
            console.log('Categories:');
            classification.categories.forEach(category => {
              console.log(
                `Name: ${category.name}, Confidence: ${category.confidence}`
              );
            });
            */

              const finalPayload = {
                  userId: payload.userId,
                  message: payload.message,
                  sourceUrl: payload.sourceUrl,
                  sentimentRawResults: sentimentResults,
                  classificationRawResults: classificationResult
              }

              console.log(finalPayload);

              const dataBuffer = Buffer.from(JSON.stringify(finalPayload));
              const topicName = "speechTatorDataPubsub";
        
              // send natural result to pubsub
              pubsubClient
                .topic(topicName)
                .publisher()
                .publish(dataBuffer)
                .then(results => {
                  const messageId = results[0];
                  console.log(`Message ${messageId} published.`);
                })
                .catch(err => {
                  console.error('ERROR:', err);
                });
          })
          .catch(err => {
            console.error('CLASSIFICATION ERROR: ', err);
          });

      })
      .catch(err => {
        console.error('SENTIMENT ANALYSIS ERROR: ', err);
      });

    // Don't forget to call the callback.
    callback();
};

